# TourGuideProject

## Description

TourGuide is SpringBoot application which permit user to find attractions close from its location, to obtains rewards and trip propositions.

## Technical specifications

TourGuide uses
* Java 1.8
* Gradle 4.8.1
* SpringBoot 2.1.6
